# define REPO_PARENT to get build environment
REPO_PARENT?=../..
-include ./common.mk

KERNELSRC ?= /lib/modules/$(shell uname -r)/build


all: driver
driver: modules

modules:
	$(MAKE) -C $(KERNELSRC) M=`pwd` GIT_VERSION=$(GIT_VERSION) CONFIG_SUPER_REPO=$(CONFIG_SUPER_REPO) CONFIG_SUPER_REPO_VERSION=$(CONFIG_SUPER_REPO_VERSION) CROSS_COMPILE=$(CROSS_COMPILE) modules

install:
	cp men_dma_kernel_test.ko $(MODS_INSTALL_DIR)

clean:
	#$(MAKE) -C $(LIN_KERNEL_DIR) M=`pwd` clean
	rm -fr *.o *.mod *.mod.c *.ko .*o.cmd Module.symvers modules.order .tmp_versions || true
