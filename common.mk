-include $(REPO_PARENT)/parent_common.mk

CPU ?= x86

# ALL_CPUS has to be at least two elements due to brace expansion
ALL_CPUS?=$(CPU) ""

GIT_VERSION = $(shell git describe --dirty --long --tags --always)

KERNELSRC?=/lib/modules/$(shell uname -r)/build

# used to convert list separated with spaces into list separated with commas
comma:= ,
empty:=
space:= $(empty) $(empty)
#used for removals
ALL_CPUS_COMMAS=$(subst $(space),$(comma),$(ALL_CPUS))

# in case mil1553 is compiled as stand alone repo
%.$(CPU).o: %.c
	$(COMPILE.c) $< $(OUTPUT_OPTION)

RANLIB?=ranlib
MKDIR?=mkdir
# use bash due to brace expansion
SHELL=/bin/bash
