/*
* Copyright (C) 2017 CERN (www.cern.ch)
* Author: Adam Wujek <adam.wujek@cern.ch>
*
* Released according to the GNU GPL, version 2 or any later version
*
* Tester of DMA transfers from the kernel space for MEN A25
*/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/seq_file.h>

#include <linux/proc_fs.h>

#include <vme4l-core.h>


#undef PFX
#define PFX "mdkt: "

static void MemDump(struct seq_file *m, uint8_t *buf, uint32_t n, uint32_t fmt);

struct proc_dir_entry *vme_root;

static unsigned int addr;
static unsigned int size;
static unsigned int space;
static unsigned int width;
static unsigned int sgl;
static unsigned int debug;

module_param( addr, uint, 0664 );
MODULE_PARM_DESC(addr, "Address to read from");
module_param( size, uint, 0664 );
MODULE_PARM_DESC(size, "Size of a transfer");
module_param( space, uint, 0664 );
MODULE_PARM_DESC(space, "VME space of a transfer");
module_param( width, uint, 0664 );
MODULE_PARM_DESC(width, "VME data width of a transfer");
module_param( sgl, uint, 0664 );
MODULE_PARM_DESC(sgl, "1=single-access+DMA");

module_param( debug, uint, 0664 );
MODULE_PARM_DESC(sgl, "Be more verbose");

void *mem = NULL;


static int mem_allocate(void)
{
	if ((mem = vmalloc(size)) == NULL) {
		return -ENOMEM;
		printk(KERN_ERR PFX "Unable to allocate mem 0x%x (%d)\n", size, size);
	}
	return 0;
}

static void mem_free(void)
{
	if (mem) {
		vfree(mem);
		mem = NULL;
	}
}


static int perform_vme_dma(int direction)
{
	int ret;
	unsigned char *pDat;
	int j;
	VME4L_RW_BLOCK blk;
	memset(&blk, 0, sizeof(blk));
	blk.vmeAddr = addr;
	blk.accWidth = width;
	blk.size = size;
	blk.flags |= VME4L_RW_KERNEL_SPACE_DMA;
	blk.direction = direction;


	if (sgl)
		blk.flags |= VME4L_RW_USE_SGL_DMA;

	blk.dataP = mem;

	if (debug) {
		printk(KERN_ERR PFX "%s: blk: space %d, vmeAddr 0x%lx, "
			"accWidth %d, size 0x%zx, dataP %p, flags %d\n",
			__func__, space, blk.vmeAddr, blk.accWidth,
			blk.size, blk.dataP, blk.flags);

		printk(KERN_ERR PFX "before transfer\n");
	}

	pDat = mem;
	if (debug) {
		for ( j = 0; j < size / 16; j++ ) {
			printk(KERN_ERR PFX "%04X | %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", j * 16,
				    pDat[0],pDat[1],pDat[2],pDat[3],pDat[4],pDat[5],pDat[6],pDat[7],
				    pDat[8],pDat[9],pDat[10],pDat[11],pDat[12],pDat[13],pDat[14],pDat[15]);

			pDat+=16;
		}
	}

	ret = vme4l_rw(space, &blk, 0);

	if (debug) {
		printk(KERN_ERR PFX "%s: returned %d\n", __func__, ret);
	}

	if (ret < 0)
		return ret;

	pDat = mem;

	if (debug) {
		for ( j = 0; j < size / 16; j++ ) {
			printk(KERN_ERR PFX "%04X | %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", j * 16,
				    pDat[0],pDat[1],pDat[2],pDat[3],pDat[4],pDat[5],pDat[6],pDat[7],
				    pDat[8],pDat[9],pDat[10],pDat[11],pDat[12],pDat[13],pDat[14],pDat[15]);

			pDat+=16;
		}
	}

	return ret;
}


/*
 * Procfs stuff
 */
static int vme_test_print(struct seq_file *m, void *data)
{
	seq_printf(m, "address: 0x%08x\n", addr);
	seq_printf(m, "size:    0x%08x\n", size);
	seq_printf(m, "space:   0x%08x\n", space);
	seq_printf(m, "width:   0x%08x\n", width);
	seq_printf(m, "sgl:     0x%08x\n", sgl);

	if (mem_allocate() < 0)
		return 0;
	perform_vme_dma(0);

	MemDump(m, mem, size, 1);

	mem_free();

	return 0;
}

static int vme_test_bin_print(struct seq_file *m, void *data)
{
	int j;

	if (mem_allocate() < 0)
		return 0;
	perform_vme_dma(0);

	for (j = 0; j < size; j++)
		seq_printf(m, "%c", *(char *)(mem + j));

	mem_free();

	return 0;
}

static int vme_test_write_print(struct seq_file *m, void *data)
{
	seq_printf(m, "Write zeros at:\n");
	seq_printf(m, "address: 0x%08x\n", addr);
	seq_printf(m, "size:    0x%08x\n", size);
	seq_printf(m, "space:   0x%08x\n", space);
	seq_printf(m, "width:   0x%08x\n", width);
	seq_printf(m, "sgl:     0x%08x\n", sgl);

	if (mem_allocate() < 0)
		return 0;
	memset(mem, 0, size);
	perform_vme_dma(1);
	MemDump(m, mem, size, 1);
	perform_vme_dma(0);

	MemDump(m, mem, size, 1);

	mem_free();

	return 0;
}

static int vme_test_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, vme_test_print, NULL);
}

static const struct file_operations vme_test_proc_ops = {
	.open		= vme_test_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int vme_test_bin_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, vme_test_bin_print, NULL);
}

static const struct file_operations vme_test_bin_proc_ops = {
	.open		= vme_test_bin_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int vme_test_write_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, vme_test_write_print, NULL);
}

static const struct file_operations vme_test_write_proc_ops = {
	.open		= vme_test_write_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

void vme_procfs_register(void)
{
	struct proc_dir_entry *entry;

	entry = proc_create("vme_test", S_IFREG | S_IRUGO, NULL, &vme_test_proc_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc vme_test node\n");

	entry = proc_create("vme_test_bin", S_IFREG | S_IRUGO, NULL, &vme_test_bin_proc_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc vme_test_bin node\n");
	
	entry = proc_create("vme_test_write", S_IFREG | S_IRUGO, NULL, &vme_test_write_proc_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc vme_test_write node\n");

}

void vme_procfs_unregister(void)
{
	remove_proc_entry("vme_test", NULL);
	remove_proc_entry("vme_test_bin", NULL);
	remove_proc_entry("vme_test_write", NULL);
}


static void print_params(void){
	printk(KERN_ERR PFX "address: 0x%08x\n", addr);
	printk(KERN_ERR PFX "size:    0x%08x\n", size);
	printk(KERN_ERR PFX "space:   0x%08x\n", space);
	printk(KERN_ERR PFX "width:   0x%08x\n", width);
	printk(KERN_ERR PFX "sgl:     0x%08x\n", sgl);
}


static int __init a25_wrapper_init(void)
{
	vme_procfs_register();
	printk(KERN_INFO PFX "Loaded\n");

	if (width == 0 || size == 0) {
		printk(KERN_ERR PFX "Invalid params to perform DMA transfer\n");
		print_params();
		return 0;

	}
	
	if ((mem = kmalloc(size, GFP_KERNEL)) == NULL) {
		return -ENOMEM;
		printk(KERN_ERR PFX "Unable to allocate mem 0x%x (%d)\n", size, size);
	}
	if (mem_allocate() < 0)
		return 0;
	perform_vme_dma(0);
	mem_free();

	return 0;
}

static void __exit a25_wrapper_exit(void)
{
	vme_procfs_unregister();
	printk(KERN_INFO PFX "Unloaded\n");
}

static void MemDump(struct seq_file *m, uint8_t *buf, uint32_t n, uint32_t fmt)
{
	uint8_t *k, *k0, *kmax = buf+n;
	int32_t i;
	seq_printf(m, "----\n");
	for (k=k0=buf; k0<kmax; k0+=16) {
		seq_printf(m, "%04x: ", (unsigned int)(k-buf));

		switch(fmt) {	/* dump hex: */
			case 4: /* long aligned */
				for (k=k0,i=0; i<16; i+=4, k+=4)
					if (k<kmax)  seq_printf(m, "%08x ",*(uint32_t*)k);
					else         seq_printf(m, "         ");
				break;
			case 2: /* word aligned */
				for (k=k0,i=0; i<16; i+=2, k+=2)
					if (k<kmax)  seq_printf(m, "%04x ",*(uint16_t*)k & 0xffff);
					else         seq_printf(m, "     ");
				break;
			default: /* byte aligned */
				for (k=k0,i=0; i<16; i++, k++)
					if (k<kmax)  seq_printf(m, "%02x ",*k & 0xff);
					else         seq_printf(m, "   ");
		}

		for (k=k0, i=0; i<16 && k<kmax; i++, k++)           /* dump ascii */
			if ( *(uint8_t*)k>=32 && *(uint8_t*)k<=127 )
				seq_printf(m, "%c", *k);
			else
				seq_printf(m, ".");

		seq_printf(m, "\n");
	}
	seq_printf(m, "----\n");
}

module_init(a25_wrapper_init);
module_exit(a25_wrapper_exit);


MODULE_AUTHOR("Adam Wujek");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("tester of DMA transfers from the kernel space for MEN A25");
MODULE_VERSION(GIT_VERSION);
